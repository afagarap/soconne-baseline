# Changelog

We track the notable changes to this project using the conventional commit messages in the `main` branch.

## Versions

- a962134 (tag: 0.5.1) chore: Bump torch to v2.0, torchvision to v0.15.1
- 7092403 (tag: v0.5.0) feat: Implement EfficientNet as a base architecture
- 79d4f8c (tag: v0.1.0) chore: import SqueezeNet at package init

## Chores

- 007d3d3 - chore: Add poetry lock file (Mon, 8 May 2023 03:31:55 +0800)
- cb5ece3 - chore: Add isort config file (Mon, 8 May 2023 03:31:45 +0800)
- 5d5b59a - chore: Update pre-commit config file (Mon, 8 May 2023 03:31:36 +0800)
- 101fbb7 - chore: Add dev dependencies (Mon, 8 May 2023 03:31:08 +0800)
- 1c37ec0 - chore: Remove primitive install method of setup.py (Mon, 8 May 2023 03:29:38 +0800)
- e7c08ee - chore: Initialize pyproject.toml (Mon, 8 May 2023 03:29:30 +0800)
- a962134 - chore: Bump torch to v2.0, torchvision to v0.15.1 (Sun, 7 May 2023 22:10:05 +0800)
- 530544e - chore: implement ensemble neural network (Mon, 16 Aug 2021 09:20:54 +0000)
- 2c9114d - chore: add model super class (Mon, 16 Aug 2021 09:12:35 +0000)
- 79d4f8c - chore: import SqueezeNet at package init (Sun, 15 Aug 2021 15:56:37 +0800)
- cfa6eea - chore: import SqueezeNet at package init (Sun, 15 Aug 2021 15:56:02 +0800)
- 43e1933 - chore: remove cuda index in DNN device (Sun, 15 Aug 2021 01:26:01 +0800)
- d02682e - chore: download pretrained cifar10 weights if not present (Thu, 12 Aug 2021 14:23:40 +0000)
- b5c1e1b - chore: import the classes at init (Fri, 6 Aug 2021 20:05:47 +0800)
- 88bae38 - chore: add pre-trained CIFAR10 ResNet models (Tue, 3 Aug 2021 13:01:52 +0000)
- 8cf313f - chore: add current SOCONNE baseline/sub-network models (Sun, 1 Aug 2021 06:34:05 +0000)
- 725f60d - chore: add .gitignore (Sun, 1 Aug 2021 14:23:20 +0800)
- 4b161cd - chore: add flake8 config (Sun, 1 Aug 2021 14:23:07 +0800)
- 3d2542d - chore: apply prettier (Sun, 1 Aug 2021 14:22:58 +0800)
- bf669be - chore: add pre-commit config (Sun, 1 Aug 2021 14:22:35 +0800)

## Documentation

- 2b99466 - docs: Link pretrained CIFAR10 models repository (Sat, 13 May 2023 22:35:03 +0800)
- 79093e6 - docs: add docstring for ResNet class functions (Tue, 10 Aug 2021 14:14:46 +0800)
- c0a42d3 - docs: add GNU AGPL v3 license (Sun, 1 Aug 2021 14:25:36 +0800)

## Features

- 7092403 - feat: Implement EfficientNet as a base architecture (Wed, 8 Sep 2021 07:52:13 +0000)
- c9f83b4 - feat: enable freezing of pretrained CIFAR10 models (Wed, 11 Aug 2021 14:21:53 +0000)

## Fixes

- 5ec354d - fix: instantiate 10-unit fc before loading pretrained weights (Mon, 23 Aug 2021 19:50:33 +0800)
- 3463bb3 - fix: resolve var issue that causes error in ResNet18 loading (Mon, 23 Aug 2021 19:21:44 +0800)
- 24d3cd7 - fix: resolve issues on pretrained CIFAR10 ResNet models for MoE (Mon, 23 Aug 2021 11:13:21 +0000)
- 681154b - fix: remove non-existent subpackage (Fri, 6 Aug 2021 20:04:08 +0800)

## Improvements

## Refactors

## Tests
