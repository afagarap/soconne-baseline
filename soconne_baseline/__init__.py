from soconne_baseline.cnn import CNN
from soconne_baseline.dnn import DNN
from soconne_baseline.efficientnet import EfficientNet
from soconne_baseline.lenet import LeNet
from soconne_baseline.resnet import ResNet18, ResNet34, ResNet50
from soconne_baseline.squeezenet import SqueezeNet
