# Repository for SOCONNE baselines/subnetworks
# Copyright (C) 2021  Abien Fred Agarap
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""Wrapper implementation of ResNet models"""
import os
from pathlib import Path
from typing import Tuple

import torch
import torchvision

from soconne_baseline.utils import download_cifar10_weights


class ResNet(torch.nn.Module):
    _state_dicts = os.path.join(str(Path.home()), "cifar10_models", "state_dicts")

    def __init__(
        self,
        device: torch.device = torch.device(
            "cuda:0" if torch.cuda.is_available() else "cpu"
        ),
        **kwargs,
    ):
        super().__init__()
        self.criterion = torch.nn.CrossEntropyLoss()
        self.device = device

    def freeze_blocks(self, num_blocks: int = 3) -> None:
        """
        Freezes the first `num_blocks` blocks of ResNet.

        Parameter
        ---------
        num_blocks: int
            The number of blocks to freeze.
        """
        assert num_blocks <= 4, "There are only 4 blocks in ResNet."
        for block_index, child in enumerate(list(self.resnet.named_children())[4:8]):
            if block_index < num_blocks:
                for param in child[1].parameters():
                    param.requires_grad = False

    def forward(self, features: torch.Tensor) -> torch.Tensor:
        """
        Defines the forward pass by the model.

        Parameter
        ---------
        features: torch.Tensor
            The input features.

        Returns
        -------
        torch.Tensor
            The model output.
        """
        return self.resnet.forward(features)

    def epoch_train(
        self, data_loader: torch.utils.data.DataLoader
    ) -> Tuple[float, float]:
        """
        Trains a model for one epoch.

        Parameters
        ----------
        data_loader: torch.utils.dataloader.DataLoader
            The data loader object that consists of the data pipeline.

        Returns
        -------
        epoch_loss: float
            The epoch loss.
        epoch_accuracy: float
            The epoch accuracy.
        """
        epoch_loss = 0
        epoch_accuracy = 0
        for batch_features, batch_labels in data_loader:
            batch_features = batch_features.to(self.device)
            batch_labels = batch_labels.to(self.device)
            self.optimizer.zero_grad()
            outputs = self(batch_features)
            train_loss = self.criterion(outputs, batch_labels)
            train_loss.backward()
            self.optimizer.step()
            epoch_loss += train_loss.item()
            accuracy = (outputs.argmax(1) == batch_labels).sum().item() / len(
                batch_labels
            )
            epoch_accuracy += accuracy
        epoch_loss /= len(data_loader)
        epoch_accuracy /= len(data_loader)
        return epoch_loss, epoch_accuracy

    def fit(
        self, data_loader: torch.utils.data.DataLoader, epochs: int, show_every: int = 2
    ) -> None:
        """
        Trains the ResNet model.

        Parameters
        ----------
        data_loader: torch.utils.dataloader.DataLoader
            The data loader object that consists of the data pipeline.
        epochs: int
            The number of epochs to train the model.
        show_every: int
            The interval in terms of epoch on displaying training progress.
        """
        for epoch in range(epochs):
            epoch_loss, epoch_accuracy = self.epoch_train(data_loader)
            self.train_loss.append(epoch_loss)
            self.train_accuracy.append(epoch_accuracy)
            if epoch % show_every == 0:
                print(
                    f"epoch {epoch + 1}/{epochs} : mean loss = {epoch_loss:.4f}\t|\tmean acc = {epoch_accuracy:.4f}"
                )

    def score(self, data_loader: torch.utils.data.DataLoader) -> float:
        """
        Returns the accuracy of the model on the given data loader.

        Parameter
        ---------
        data_loader: torch.utils.data.DataLoader
            The data loader to use for model evaluation.

        Returns
        -------
        accuracy: float
            The accuracy of the model on the given data loader.
        """
        self.eval()
        self.device = torch.device("cpu")
        self.to(self.device)
        with torch.no_grad():
            for features, labels in data_loader:
                outputs = self(features)
        accuracy = (outputs.argmax(1) == labels).sum().item() / len(labels)
        accuracy *= 100.0
        return accuracy


class ResNet18(ResNet):
    _pretrained_cifar10_weights = "resnet18.pt"

    def __init__(
        self,
        input_shape: Tuple,
        num_classes: int,
        learning_rate: float = 1e-3,
        weight_decay: float = 1e-5,
        blocks_to_freeze: int = 3,
        use_pretrained_cifar10: bool = False,
        device: torch.device = torch.device(
            "cuda:0" if torch.cuda.is_available() else "cpu"
        ),
    ):
        """
        Loads a pretrained ResNet18 classifier.

        Parameters
        ----------
        num_classes: int
            The number of classes in the dataset.
        learning_rate: float
            The learning rate to use for optimization.
        device: torch.device
            The device to use for computations.
        """
        super().__init__()
        if use_pretrained_cifar10:
            if not os.path.isfile(
                os.path.join(ResNet._state_dicts, ResNet18._pretrained_cifar10_weights)
            ):
                download_cifar10_weights()
            self.resnet = torchvision.models.resnet18()
            self.resnet.conv1 = torch.nn.Conv2d(
                in_channels=3,
                out_channels=self.resnet.conv1.out_channels,
                kernel_size=(3, 3),
                stride=(1, 1),
                padding=(1, 1),
                bias=False,
            )
            if num_classes != 10:
                self.resnet.fc = torch.nn.Linear(
                    in_features=self.resnet.fc.in_features, out_features=10
                )
                self.resnet.load_state_dict(
                    torch.load(
                        os.path.join(
                            ResNet._state_dicts, ResNet18._pretrained_cifar10_weights
                        )
                    )
                )
                self.resnet.fc = torch.nn.Linear(
                    in_features=self.resnet.fc.in_features, out_features=num_classes
                )
            elif num_classes == 10:
                self.resnet.fc = torch.nn.Linear(
                    in_features=self.resnet.fc.in_features, out_features=10
                )
                self.resnet.load_state_dict(
                    torch.load(
                        os.path.join(
                            ResNet._state_dicts, ResNet18._pretrained_cifar10_weights
                        )
                    )
                )
            self.freeze_blocks(num_blocks=blocks_to_freeze)
            print(
                f"[INFO] Loaded pretrained CIFAR10 model from {os.path.join(ResNet._state_dicts, ResNet18._pretrained_cifar10_weights)}"
            )
        else:
            self.resnet = torchvision.models.resnet.resnet18(pretrained=True)
            if len(input_shape) < 4:
                self.resnet.conv1 = torch.nn.Conv2d(
                    1, 64, kernel_size=7, stride=2, padding=3, bias=False
                )
            self.freeze_blocks(num_blocks=blocks_to_freeze)
            self.resnet.fc = torch.nn.Linear(
                in_features=self.resnet.fc.in_features, out_features=num_classes
            )
        self.optimizer = torch.optim.SGD(
            params=(
                filter(lambda parameters: parameters.requires_grad, self.parameters())
            ),
            momentum=9e-1,
            lr=learning_rate,
            weight_decay=weight_decay,
        )
        self.resnet.to(self.device)


class ResNet34(ResNet):
    _pretrained_cifar10_weights = "resnet34.pt"

    def __init__(
        self,
        input_shape: Tuple,
        num_classes: int,
        learning_rate: float = 1e-3,
        weight_decay: float = 1e-5,
        blocks_to_freeze: int = 3,
        use_pretrained_cifar10: bool = False,
        device: torch.device = torch.device(
            "cuda:0" if torch.cuda.is_available() else "cpu"
        ),
    ):
        """
        Loads a pretrained ResNet34 classifier.

        Parameters
        ----------
        num_classes: int
            The number of classes in the dataset.
        learning_rate: float
            The learning rate to use for optimization.
        device: torch.device
            The device to use for computations.
        """
        super().__init__()
        if use_pretrained_cifar10:
            if not os.path.isfile(
                os.path.join(ResNet._state_dicts, ResNet34._pretrained_cifar10_weights)
            ):
                download_cifar10_weights()
            self.resnet = torchvision.models.resnet34()
            self.resnet.conv1 = torch.nn.Conv2d(
                in_channels=3,
                out_channels=self.resnet.conv1.out_channels,
                kernel_size=(3, 3),
                stride=(1, 1),
                padding=(1, 1),
                bias=False,
            )
            if num_classes != 10:
                self.resnet.fc = torch.nn.Linear(
                    in_features=self.resnet.fc.in_features, out_features=10
                )
                self.resnet.load_state_dict(
                    torch.load(
                        os.path.join(
                            ResNet._state_dicts, ResNet34._pretrained_cifar10_weights
                        )
                    )
                )
                self.resnet.fc = torch.nn.Linear(
                    in_features=self.resnet.fc.in_features, out_features=num_classes
                )
            elif num_classes == 10:
                self.resnet.fc = torch.nn.Linear(
                    in_features=self.resnet.fc.in_features, out_features=10
                )
                self.resnet.load_state_dict(
                    torch.load(
                        os.path.join(
                            ResNet._state_dicts, ResNet34._pretrained_cifar10_weights
                        )
                    )
                )
            self.freeze_blocks(num_blocks=blocks_to_freeze)
            print(
                f"[INFO] Loaded pretrained CIFAR10 model from {os.path.join(ResNet._state_dicts, ResNet34._pretrained_cifar10_weights)}"
            )
        else:
            self.resnet = torchvision.models.resnet.resnet34(pretrained=True)
            if len(input_shape) < 4:
                self.resnet.conv1 = torch.nn.Conv2d(
                    1, 64, kernel_size=7, stride=2, padding=3, bias=False
                )
            self.freeze_blocks(num_blocks=blocks_to_freeze)
            self.resnet.fc = torch.nn.Linear(
                in_features=self.resnet.fc.in_features, out_features=num_classes
            )
        self.optimizer = torch.optim.SGD(
            params=(
                filter(lambda parameters: parameters.requires_grad, self.parameters())
            ),
            momentum=9e-1,
            lr=learning_rate,
            weight_decay=weight_decay,
        )
        self.resnet.to(self.device)


class ResNet50(ResNet):
    _pretrained_cifar10_weights = "resnet50.pt"

    def __init__(
        self,
        input_shape: Tuple,
        num_classes: int,
        learning_rate: float = 1e-3,
        weight_decay: float = 1e-5,
        blocks_to_freeze: int = 3,
        use_pretrained_cifar10: bool = False,
        device: torch.device = torch.device(
            "cuda:0" if torch.cuda.is_available() else "cpu"
        ),
    ):
        """
        Loads a pretrained ResNet50 classifier.

        Parameters
        ----------
        num_classes: int
            The number of classes in the dataset.
        learning_rate: float
            The learning rate to use for optimization.
        device: torch.device
            The device to use for computations.
        """
        super().__init__()
        if use_pretrained_cifar10:
            if not os.path.isfile(
                os.path.join(ResNet._state_dicts, ResNet50._pretrained_cifar10_weights)
            ):
                download_cifar10_weights()
            self.resnet = torchvision.models.resnet50()
            self.resnet.conv1 = torch.nn.Conv2d(
                in_channels=3,
                out_channels=self.resnet.conv1.out_channels,
                kernel_size=(3, 3),
                stride=(1, 1),
                padding=(1, 1),
                bias=False,
            )
            if num_classes != 10:
                self.resnet.fc = torch.nn.Linear(
                    in_features=self.resnet.fc.in_features, out_features=10
                )
                self.resnet.load_state_dict(
                    torch.load(
                        os.path.join(
                            ResNet._state_dicts, ResNet50._pretrained_cifar10_weights
                        )
                    )
                )
                self.resnet.fc = torch.nn.Linear(
                    in_features=self.resnet.fc.in_features, out_features=num_classes
                )
            elif num_classes == 10:
                self.resnet.fc = torch.nn.Linear(
                    in_features=self.resnet.fc.in_features, out_features=10
                )
                self.resnet.load_state_dict(
                    torch.load(
                        os.path.join(
                            ResNet._state_dicts, ResNet50._pretrained_cifar10_weights
                        )
                    )
                )
            self.freeze_blocks(num_blocks=blocks_to_freeze)
            print(
                f"[INFO] Loaded pretrained CIFAR10 model from {os.path.join(ResNet._state_dicts, ResNet50._pretrained_cifar10_weights)}"
            )
        else:
            self.resnet = torchvision.models.resnet.resnet50(pretrained=True)
            if len(input_shape) < 4:
                self.resnet.conv1 = torch.nn.Conv2d(
                    1, 64, kernel_size=7, stride=2, padding=3, bias=False
                )
            self.freeze_blocks(num_blocks=blocks_to_freeze)
            self.resnet.fc = torch.nn.Linear(
                in_features=self.resnet.fc.in_features, out_features=num_classes
            )
        self.optimizer = torch.optim.SGD(
            params=(
                filter(lambda parameters: parameters.requires_grad, self.parameters())
            ),
            momentum=9e-1,
            lr=learning_rate,
            weight_decay=weight_decay,
        )
        self.resnet.to(self.device)
