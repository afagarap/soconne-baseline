"""Implementation of ensemble neural networks"""
from copy import deepcopy
from typing import Dict, Tuple

import torch
import torchvision
from soconne.models import CNN, Model
from soconne.models.squeezenet import SqueezeNet

__author__ = "Abien Fred Agarap"


class EnsembleNN(Model):
    def __init__(
        self,
        num_features: int,
        input_shape: Tuple,
        use_feature_extractor: bool = False,
        feature_extractor_arch: str = "dnn",
        code_dim: int = 256,
        subnetwork: torch.nn.Module = None,
        num_subnetworks: int = 3,
        optimizer: str = "sgd",
        learning_rate: float = 1e-3,
        weight_decay: float = 1e-5,
        device: torch.device = torch.device(
            "cuda:0" if torch.cuda.is_available() else "cpu"
        ),
    ):
        """
        Constructs an ensemble of neural networks.

        Parameters
        ----------
        network: torch.nn.Module:
            The learner architecture to use in ensemble.
        num_learners: int
            The number of networks to use in ensemble.
        use_feature_extractor: bool
            Whether to use a feature extractor or not.
        feature_extractor_arch: str
            The architecture to use for feature extractor.
        learning_rate: float
            The learning rate to use for optimization.
        weight_decay: float
            The weight decay parameter to use.
        device: torch.device
            The device to use for computations.
        """
        super().__init__()
        self.subnetworks = torch.nn.Sequential()
        for index in range(num_subnetworks):
            subnetwork = subnetwork.to(self.device)
            subnetwork = deepcopy(subnetwork)
            if not subnetwork.__class__.__name__.lower().__contains__("resnet"):
                subnetwork.apply(self.reset_parameters)
            self.subnetworks.add_module(f"network_{index}", subnetwork)
        if optimizer == "sgd":
            self.optimizer = torch.optim.SGD(
                params=filter(
                    lambda parameters: parameters.requires_grad, self.parameters()
                ),
                lr=learning_rate,
                momentum=9e-1,
                weight_decay=weight_decay,
            )
            self.scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(
                self.optimizer, patience=2, verbose=True, min_lr=1e-4, factor=1e-1
            )
        elif optimizer == "adamw":
            self.optimizer = torch.optim.AdamW(
                params=filter(
                    lambda parameters: parameters.requires_grad, self.parameters()
                ),
                lr=learning_rate,
                weight_decay=weight_decay,
            )
            self.scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(
                self.optimizer, patience=1, verbose=True, min_lr=1e-4, factor=1e-2
            )
        self.device = device
        self.to(self.device)

    def forward(self, features: torch.Tensor) -> torch.Tensor:
        """
        The forward pass by the model.

        Parameter
        ---------
        features: torch.Tensor
            The input features.

        Returns
        -------
        logits: torch.Tensor
            The model outputs.
        """
        if self.use_feature_extractor:
            features = self.feature_extractor(features)
        outputs = []
        for subnetwork in self.subnetworks:
            outputs.append(subnetwork(features))
        outputs = torch.stack(outputs, dim=1)
        logits = torch.mean(outputs, dim=1)
        return logits
