import os
import time
from copy import deepcopy
from typing import Dict, Tuple

import torch


class Model(torch.nn.Module):
    def __init__(self):
        super().__init__()

    def forward(self, features: torch.Tensor) -> torch.Tensor:
        raise NotImplementedError

    def epoch_train(self, data_loaders: Dict, phase: str) -> Tuple[float, float]:
        """
        Performs single epoch training for model.

        Parameters
        ----------
        data_loaders: Dict
            The data loaders to use for
            training and validation.
        phase: str
            The phase of training,
            whether training or validation.

        Returns
        -------
        Tuple[float, float]
            epoch_loss
                The training or validation epoch loss.
            epoch_accuracy
                The training or validation epoch accuracy.
        """
        epoch_loss = 0.0
        epoch_accuracy = 0.0
        for features, labels in data_loaders.get(phase):
            features = features.to(self.device)
            labels = labels.to(self.device)

            self.optimizer.zero_grad()

            with torch.set_grad_enabled(phase == "train"):
                outputs = self(features)
                loss = self.criterion(outputs, labels)

                if phase == "train":
                    loss.backward()
                    self.optimizer.step()

            epoch_loss += loss.item()
            epoch_accuracy += (outputs.argmax(1) == labels).sum().item() / len(labels)
        epoch_loss /= len(data_loaders.get(phase))
        epoch_accuracy /= len(data_loaders.get(phase))
        return epoch_loss, epoch_accuracy

    def fit(
        self,
        train_loader: torch.utils.data.DataLoader,
        valid_loader: torch.utils.data.DataLoader,
        epochs: int,
        show_every: int = 2,
    ) -> None:
        """
        Trains the model for a given epochs.

        Parameters
        ----------
        train_loader: torch.utils.data.DataLoader
            The training data loader to use.
        valid_loader: torch.utils.data.DataLoader
            The validation data loader to use.
        epochs: int
            The number of epochs to train the model.
        show_every: int
            The interval between training results displays.
        """
        start_time = time.time()
        best_model_weights = deepcopy(self.state_dict())
        best_accuracy = 0.0

        data_loaders = {"train": train_loader, "valid": valid_loader}

        for epoch in range(epochs):
            if (epoch + 1) % show_every == 0:
                print()
                print(f"Epoch {epoch + 1}/{epochs}")
                print("-" * 40)
            for phase in data_loaders.keys():
                if phase == "train":
                    self.train()
                else:
                    self.eval()

                epoch_loss, epoch_accuracy = self.epoch_train(
                    data_loaders=data_loaders, phase=phase
                )

                if phase == "train":
                    self.scheduler.step(epoch)

                if phase == "train":
                    self.train_loss.append(epoch_loss)
                    self.train_accuracy.append(epoch_accuracy)
                elif phase == "valid":
                    self.valid_loss.append(epoch_loss)
                    self.valid_accuracy.append(epoch_accuracy)

                if (epoch + 1) % show_every == 0:
                    print(
                        f"{phase.title()} Loss: {epoch_loss:.4f} Acc: {epoch_accuracy:.4f}"
                    )

                if phase == "valid" and epoch_accuracy > best_accuracy:
                    best_accuracy = epoch_accuracy
                    best_model_weights = deepcopy(self.state_dict())

        time_elapsed = time.time() - start_time
        print()
        print(
            f"Training complete in {(time_elapsed // 60):.0f}m {(time_elapsed % 60):.0f}s"
        )
        print(f"Best Validation Accuracy: {best_accuracy:.4f}")
        print()

        self.load_state_dict(best_model_weights)

    def predict(self, features: torch.Tensor) -> torch.Tensor:
        """
        Computes the model outputs.

        Parameter
        ---------
        features: torch.Tensor
            The input features.

        Returns
        -------
        outputs: torch.Tensor
            The model outputs.
        """
        outputs = self(features)
        return outputs

    def score(self, data_loader: torch.utils.data.DataLoader) -> float:
        """
        Computes the accuracy of the model.

        Parameter
        ---------
        data_loader: torch.utils.data.DataLoader
            The data loader to use in evaluating the model.

        Returns
        -------
        accuracy: float
            The model accuracy.
        """
        self.eval()
        if not torch.cuda.get_device_properties(0).name.__contains__("V100"):
            self.device = torch.device("cpu")
            self.to(self.device)
        with torch.no_grad():
            for features, labels in data_loader:
                features = features.to(self.device)
                labels = labels.to(self.device)
                outputs = self.predict(features)
                correct = (outputs.argmax(1) == labels).sum().item()
                accuracy = correct / len(labels)
        accuracy *= 100.0
        return accuracy

    def save_model(self, filename: str) -> None:
        """
        Exports the trained model to
        `outputs/models` directory.

        Parameter
        ---------
        filename: str
            The filename for the exported model.
        """
        print("[INFO] Exporting trained model...")
        model_name = filename.split("-")[2]
        dataset_name = filename.split("-")[5]
        model_path = os.path.join("outputs", "models", model_name, dataset_name)
        if not os.path.exists(model_path):
            os.makedirs(model_path)
        filename = f"{filename}.pth"
        filename = os.path.join(model_path, filename)
        torch.save(self.state_dict(), filename)
        print(f"[SUCCESS] Trained model exported to {filename}")

    def load_model(self, filename: str) -> None:
        """
        Loads the trained model from
        `outputs/models` directory.

        Parameter
        ---------
        filename: str
            The filename for the exported model to load.
        """
        print("[INFO] Loading the trained model...")
        model_name = filename.split("-")[2]
        dataset_name = filename.split("-")[5]
        model_path = os.path.join("outputs", "models", model_name, dataset_name)
        if not filename.endswith(".pth"):
            filename = f"{filename}.pth"
        filename = os.path.join(model_path, filename)
        if os.path.isfile(filename):
            self.load_state_dict(torch.load(filename))
            print("[SUCCESS] Trained model ready for use.")
        else:
            print("[ERROR] Trained model not found.")
