# Repository for SOCONNE baselines/subnetworks
# Copyright (C) 2021  Abien Fred Agarap
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""Utility functions module"""
import os
import zipfile
from pathlib import Path

import requests
from tqdm import tqdm


def download_cifar10_weights() -> None:
    """
    Downloads the pretrained CIFAR10 weights if they
    are not yet in `~/cifar10_models`.
    """
    url = "https://rutgers.box.com/shared/static/gkw08ecs797j2et1ksmbg1w5t3idf5r5.zip"
    response = requests.get(url, stream=True)
    total_size = int(response.headers.get("content-length", 0))
    block_size = 2 ** 20
    progress = tqdm(total=total_size, unit="iB", unit_scale=True)

    with open("state_dicts.zip", "wb") as file:
        for data in response.iter_content(block_size):
            progress.update(len(data))
            file.write(data)
    progress.close()

    if total_size != 0 and progress.n != total_size:
        raise Exception("[ERROR] An unexpected error had occurred.")

    print("[INFO] Download successful. Unzipping file...")
    path = str(Path.home())
    path = os.path.join(path, "cifar10_models")
    zip_file = os.path.join(path, "state_dicts.zip")
    with zipfile.ZipFile(zip_file, "r") as file:
        file.extractall(path)
        print(f"[INFO] Extracted models to {path}")
