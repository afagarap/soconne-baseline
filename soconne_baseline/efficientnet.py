import torch
from efficientnet_pytorch import EfficientNet as EfficientNetPt


class EfficientNet(torch.nn.Module):
    _supported_models = [f"efficientnet-b{index}" for index in range(8)]

    def __init__(
        self,
        model: str = "efficientnet-b0",
        num_classes: int = 10,
        freeze_parameters: bool = False,
        device: torch.device = torch.device(
            "cuda" if torch.cuda.is_available() else "cpu"
        ),
    ):
        """
        Loads a pretrained EfficientNet model.

        Parameters
        ----------
        model: str
            The EfficientNet model variant to load.
        num_classes: int
            The number of classes to train the model on.
        freeze_parameters: bool
            Whether to freeze the pretrained model or not.
        device: torch.device
            The device to use for computation.
        """
        self.model = EfficientNetPt.from_pretrained(model)
        if freeze_parameters:
            self.freeze_parameters()
        self.model._fc = torch.nn.Linear(
            in_features=self.model._fc, out_features=num_classes
        )
        self.device = device
        self.to(self.device)

    def freeze_parameters(self):
        """
        Freezes the model parameters.
        """
        for parameter in self.model.parameters():
            parameter.requires_grad = False

    def forward(self, features: torch.Tensor) -> torch.Tensor:
        """
        The forward pass by the model.

        Parameter
        ---------
        features: torch.Tensor
            The input features.

        Returns
        -------
        outputs: torch.Tensor
            The model logits.
        """
        outputs = self.model(features)
        return outputs
