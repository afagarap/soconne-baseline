# Soconne Baseline

An implementation of baseline/sub-network models for Self-Organizing Cooperative Neural Network Experts (SOCONNE)

## License

This repository **may not be used** for commercial purposes.
